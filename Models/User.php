<?php
class User{
    private $id;
    private $username;
    private $pass;
    private $firstname;
    private $lastname;

    public function __construct($id, $username,$pass, $firstname, $lastname){
        $this->id = $id;
        $this->username = $username;
        $this->pass = $pass;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
    }



    public function getId()
    {
        return $this->id;
    }


    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPass()
    {
        return $this->pass;
    }

    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }


}