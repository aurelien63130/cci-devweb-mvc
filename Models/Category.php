<?php
// Classe qui représente une catégorie en BDD
// Tous les attributs correspondent à
// une colonne de la table Category
class Category{

    // L'attribut privé $id
    // qui correspond à notre id en BDD
    private $id;

    // L'attribut privé $label
    // qui représente la colonne label de notre BDD
    private $label;

    // Le constructeur qui permet d'initialiser nos objets
    // Il prend 2 paramétre et assigne les valeurs de nos attributs
    public function __construct($id, $label){
        $this->id = $id;
        $this->label = $label;
    }

    // Les accesseurs et mutateurs (get - set)
    // Ils permettent de réccupérer ou modifier
    // les valeurs de nos attributs privés
    public function getId(){
        return $this->id;
    }

    public function getLabel(){
        return $this->label;
    }

    public function setLabel($label){
        $this->label = $label;
    }
}