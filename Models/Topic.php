<?php
class Topic{
    private $id;
    private $author;
    private $dateAdd;
    private $titre;
    private $image;
    private $contenu;

    public function __construct($id, $author, $dateAdd, $titre, $image, $contenu)
    {
        $this->id = $id;
        $this->author = $author;
        $this->dateAdd = date_create($dateAdd);
        $this->titre = $titre;
        $this->image = $image;
        $this->contenu = $contenu;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    public function getDateAdd()
    {
        return $this->dateAdd;
    }

    public function setDateAdd($dateAdd)
    {
        $this->dateAdd = $dateAdd;
    }

    public function getTitre()
    {
        return $this->titre;
    }

    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getContenu()
    {
        return $this->contenu;
    }

    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }
}