<?php
    class UserManager extends DbManager{
        public function getByUsername($username){
            $retour = null;
            $query = $this->bdd->prepare("SELECT * FROM user WHERE username = :username");
            $query->execute([
                "username"=> $username
            ]);

            $result = $query->fetch();

            if($result){
                $retour = new User($result["id"], $result["username"], $result["pass"], $result["firstname"], $result["lastname"]);
            }

            return $retour;
        }

        public function add($user){
            $query = $this->bdd->prepare("INSERT INTO user ( username, pass, firstname, lastname) VALUES (:username, :pass, :firstname, :lastname)");
            $query->execute([
                "username"=> $user->getUsername(),
                "pass"=> $user->getPass(),
                "firstname"=> $user->getFirstname(),
                "lastname"=> $user->getLastname()
            ]);
        }
    }
?>