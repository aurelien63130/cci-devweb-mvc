<?php
class TopicManager extends DbManager {

   public function getAll(){
       $arrayReturn = [];
       $req = $this->bdd->prepare("SELECT * FROM topic");
       $req->execute();

       $res = $req->fetchAll();

       foreach ($res as $result){
            $arrayReturn[] = new Topic($result["id"], $result["author"], $result["date_add"], $result["titre"], $result["image"], $result["contenu"]);
       }

       return $arrayReturn;
   }

   public function getOne($id){
       $retour = null;

       $req = $this->bdd->prepare("SELECT * FROM topic WHERE id = :id");

       $req->execute([
          "id"=> $id
       ]);

       $resultat = $req->fetch();

       if($resultat){
           $retour = new Topic($resultat["id"], $resultat["author"],
               $resultat["date_add"], $resultat["titre"], $resultat["image"], $resultat["contenu"]
           );
       }

       return $retour;
   }

    public function delete($id)
    {
        $req = $this->bdd->prepare("DELETE FROM topic WHERE id = :id");

        $req->execute([
            "id"=> $id
        ]);

    }

    public function add(Topic $article)
    {
        $req = $this->bdd->prepare("INSERT INTO topic 
    (author, date_add, titre, image, contenu) VALUES (:author, :date_add, :titre, :image, :contenu)");

        $req->execute([
            "author"=> $article->getAuthor(),
            "date_add"=> $article->getDateAdd()->format("Y-m-d"),
            "titre"=> $article->getTitre(),
            "image"=> $article->getImage(),
            "contenu"=> $article->getContenu()
        ]);
    }


}