<?php
    class DbManager {
        protected $bdd;

        private $host = 'database';
        private $database = 'cci-forum';
        private $username= 'root';
        private $password = 'tiger';

        public function __construct(){
            try {
                $this->bdd = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->database . ';charset=UTF8', $this->username, $this->password);
                $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(Exception $ex) {
                die('Error '. $ex->getMessage());
            }
        }
    }
?>