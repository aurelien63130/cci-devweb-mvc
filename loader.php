<?php
require 'Models/Category.php';
require 'Models/User.php';
require 'Models/Topic.php';
require 'Models/Managers/DbManager.php';
require 'Models/Managers/CategoryManager.php';
require 'Models/Managers/TopicManager.php';
require 'Models/Managers/UserManager.php';
require "Controllers/AuthenticatedController.php";
require 'Controllers/CategoryController.php';
require 'Controllers/SecurityController.php';
require 'Controllers/TopicController.php';
?>