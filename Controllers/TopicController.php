<?php
class TopicController extends AuthenticatedController {

    private $topicManager;

    public function __construct()
    {
        parent::__construct();
        $this->topicManager = new TopicManager();
    }

    public function listTopic(){

        $topics = $this->topicManager->getAll();

        require 'Views/topics/list.php';
    }

    public function detail($id){
        $topic = $this->topicManager->getOne($id);

         require 'Views/topics/detail.php';

    }

    public function delete($id)
    {
        $this->topicManager->delete($id);

        header("Location: index.php?controller=topic&action=list");
    }

    public function add()
    {
        $errors = [];

        if($_SERVER["REQUEST_METHOD"] == "POST"){

            if(empty($_POST["titre"])){
                $errors[] = "Veuillez saisir un titre";
            }

            if(empty($_POST["contenu"])){
                $errors[] = "Veuillez saisir un contenu";
            }
            // UPLOAD : 1 vérifier que l'utilisateur peut uploader ce type de fichier
            $allowedFile = ["image/jpeg", 'image/png'];
            if(!in_array($_FILES["image"]["type"], $allowedFile)){
                $errors[] = "Tu n'as pas le droit nous prenons que des JPG ou PNG";
            }

            // Vérifier que le fichier n'est pas trop lourd

            if($_FILES["image"]["size"]> 2097152){
                $errors[] = "Votre fichier est trop lourd pour nos petits serveurs";
            }

            // On verifie que error est égal à 0
            if($_FILES["image"]["error"] != 0){
                $errors[] = "Une erreur s'est produite lors de l'upload";
            }

            if(count($errors) == 0){
                // Générer un nom de fichier unique
                $fileName = uniqid().'.'.explode("/", $_FILES["image"]["type"])[1];
                // Upload de notre fichier
                move_uploaded_file($_FILES["image"]["tmp_name"], "Public/uploads/".$fileName);

                $article = new Topic(null, $this->user->getFirstname().' '. $this->user->getLastname(),
                date('Y-m-d'), $_POST["titre"], $fileName, $_POST["contenu"]);

                $this->topicManager->add($article);

                header("Location: index.php?controller=topic&action=list");
            }

        }

        require "Views/topics/add.php";


    }

}