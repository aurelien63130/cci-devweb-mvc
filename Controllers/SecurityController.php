<?php
class SecurityController{

    private $userManager;

    public function __construct(){
        $this->userManager = new UserManager();
    }

    public function login(){
        $errors = [];

        if($_SERVER["REQUEST_METHOD"] == 'POST'){
            // Pas de saisie du username
            if(empty($_POST["username"])){
                $errors[] = "Veuillez saisir un nom d'utilisateur";
            }

            // Pas de saisie du mot de passe
            if(empty($_POST["password"])){
                $errors[] = "Veuillez saisir un mot de passe";
            }

            // Identifiants incorrects !
            $user = $this->userManager->getByUsername($_POST["username"]);
            if(is_null($user) || !password_verify($_POST["password"], $user->getPass())){
                $errors[] = "Les identifiants ne sont pas valide";
            }

            if(count($errors) == 0){
                $_SESSION["user"] = serialize($user);
                header("Location: index.php");
            }
        }

        require "Views/security/login.php";
    }

    public function logout(){
        session_unset();
        session_destroy();
        header("Location: index.php?controller=security&action=login");

    }

    public function register(){
        $errors = [];

        if($_SERVER["REQUEST_METHOD"] == "POST"){
            // On vérifie que l'utilisateur n'existe pas déjà
            if(!is_null($this->userManager->getByUsername($_POST["username"]))){
                $errors[]  = "Un compte avec ce nom d'utilisateur existe déjà";
            }

            // On vérifie que la confirmation du mot de passe est égale au mot de passe
            if($_POST["password_1"] != $_POST["password_confirm"]){
                $errors[] = "Les mots de passe ne sont pas identiques";
            }

            // On vérifie que l'utilisateur a saisie un lastname
            if(empty($_POST["lastname"])){
                $errors[] = 'Veuillez saisir votre prénom';
            }

            // On vérifie que l'utilisateur a saisi un lastname
            if(empty($_POST["firstname"])){
                $errors[] = 'Veuillez saisir votre nom de famille';
            }

            // Si on a pas d'erreur on ajoute notre utilisateur en BDD
            if(count($errors) == 0){
                $hash = password_hash($_POST["password_1"], PASSWORD_DEFAULT);
                $user = new User(null, $_POST["username"], $hash, $_POST["firstname"], $_POST["lastname"]);

                $this->userManager->add($user);

                header("Location: index.php?controller=security&action=login");
            }
        }

       require 'Views/security/register.php';
    }
}