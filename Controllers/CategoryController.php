<?php
class CategoryController extends AuthenticatedController {

    // Cet attribut qui sera initialisé dans le
    // constructeur sera un objet CategoryManager
    private $categoryManager;

    // Appelé lors de l'instanciation d'un objet
    // CategoryController.
    public function __construct(){
        parent::__construct();
        // A l'initialisation de notre objet,
        // on indique que categoryManager sera un
        // nouvel objet de type CategoryManager
        $this->categoryManager = new CategoryManager();
    }

    // La méthode listCategory selectionne et affiche
    // toutes les données de la table category dans un
    // tableau. Elle fait appel au model puis affiche
    // une vue
    public function listCategory(){

        // On appel notre manager pour réccupérer les
        // données sous forme de tableau d'objet
        $categories = $this->categoryManager->getAll();

        // On inclu la vue du listing
        // Elle sera composé essentiellement de HTML
        // Elle affichera sous forme de tableau
        // les catégories contenu dans la variable $categories
        require 'Views/category/list.php';
    }

    // Prend un paramètre
    // Appelée par le routeur afin de supprimer une category
    public function deleteCategory($id){
        // Appel notre manager et sa méthode delete en lui
        // passant l'id à supprimer
        $this->categoryManager->delete($id);

        // On redirige l'utilisateur vers le listing une fois la categ supprimé
        header("Location: index.php?controller=category&action=list");

    }

    // La méthode form aura 2 buts :
    // Afficher le formulaire
    // Traité les données du formulaire
    // Si les données sont valides, elle enregistrera les données
    public function form(){
        // Variable qui correspondra aux erreurs de saisies utilisateur
        $errors = [];

        // On passera dans ce if seulement si l'on a soumis notre formulaire
        if($_SERVER["REQUEST_METHOD"] == 'POST'){
            // On vérifie ici si l'on a des erreurs de saisie
            if(empty($_POST["label"])){
                // Si il y en a on rempli le tableau défini à la ligne 52
                $errors[] = 'Veuillez entrer un nom !';
            }

            // Si la taille du tableau est égale à 0, il n'y a pas d'erreur
            // de saisie, je peux enregistrer mes données
            if(count($errors) == 0){
               // Je cré un nouvel objet catégorie à partir des données
                // de ma requête
               $category = new Category(null, $_POST["label"]);

               // J'appel mon manager pour ajouter cet element
                // la méthode add de mon manager prend en paramètre un
                // objet de type Category
               $this->categoryManager->add($category);

               // Je redirige l'utilisateur vers le listing
               header("Location: index.php?controller=category&action=list");
            }
        }

        // On affiche la vue du formulaire
        require 'Views/category/add.php';
    }


    public function detail($id)
    {
        // Je séléctionne l'objet catégorie que je veux afficher
        // dans mon manager
        $categ = $this->categoryManager->getOne($id);

        // J'affiche ma vue de détail
        require 'Views/category/detail.php';
    }


    public function edit($id)
    {
        // On déclare un tableau vide qui contiendra plus tard
        // les erreurs de saisie utilisateur
        $errors = [];

        // Utilise notre manager pour selectionner la categorie à mettre à jour
        // On s'en servira pour pré-remplir notre formulaire
        // Elle prend en paramètre une variable $id et retourne l'objet associé
        $category = $this->categoryManager->getOne($id);

        // On vérifie si on a ici une soumission de formulaire
        if($_SERVER["REQUEST_METHOD"] == "POST"){
            // On vérifie les données saisies dans notre formulaire
            if(empty($_POST["label"])){
                $errors[] = 'Veuillez saisir un label de category';
            }

            // On utilise les mutateurs pour modifier l'objet category
            // réccupéré à la ligne 104
            $category->setLabel($_POST["label"]);

            // On appel notre manager pour mettre à jour notre objet Category
            $this->categoryManager->edit($category);

            // On redirige l'utilisateur vers le listing
            header("Location: index.php?controller=category&action=list");

        }

        // Sinon on affiche notre vue (on y retrouvera le listing et les erreurs de saisie utilisateur)
        require 'Views/category/edit.php';
    }
}