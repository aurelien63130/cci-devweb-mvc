<html>
<head>
    <link rel="stylesheet" href="Public/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="Public/css/awesome/css/all.css"/>
</head>
<body>
<div class="container">
    <?php
        include "Views/parts/menu.php";
    ?>
    <h1>Se connecter !</h1>

    <form method="post">
        <div class="form-group">
            <label for="username">Username</label>
            <input id="username" name="username" class="form-control"  type="text">
        </div>

        <div class="form-group">
            <label for="password">Mot de passe</label>
            <input id="password" name="password" class="form-control"  type="password">


        </div>

        <input class="mt-3 btn btn-success" type="submit">

        <?php

        foreach ($errors as $error){
            echo('<div class="alert alert-danger mt-3" role="alert">
  '.$error.'
</div>');
        }
        ?>
    </form>
</div>

<script rel="script" src="Public/js/bootstrap.min.js"></script>
</body>
</html>