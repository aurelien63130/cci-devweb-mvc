<html>
<head>
    <link rel="stylesheet" href="Public/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="Public/css/awesome/css/all.css"/>
</head>
<body>
    <div class="container">
        <?php
        include "Views/parts/menu.php"
        ?>
        <h1>La liste de mes catégories !</h1>

        <a href="index.php?controller=category&action=ajout">Ajouter une catégorie</a>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Label</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>

            <?php
                // Je parcours mon tableau d'objet pour afficher une ligne à chaque fois
                foreach ($categories as $category){
                    // On utilise les getteurs pour accèder aux attributs privé
                    echo(' <tr>
                <th scope="row">'.$category->getId().'</th>
                <td>'.$category->getLabel().'</td>
                <td>
                    <a title="Supprimer"  href="index.php?controller=category&action=delete&id='.$category->getId().'">
                        <i class="fa fa-trash"></i>
                    </a>
                    
                       <a title="Détail"  href="index.php?controller=category&action=detail&id='.$category->getId().'">
                        <i class="fa fa-eye"></i>
                    </a>
                    
                      <a title="Editer"  href="index.php?controller=category&action=edit&id='.$category->getId().'">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
            </tr>');
                }
            ?>

            </tbody>
        </table>
    </div>

    <script rel="script" src="Public/js/bootstrap.min.js"></script>
</body>
</html>