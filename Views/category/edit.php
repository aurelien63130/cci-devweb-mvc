<html>
<head>
    <link rel="stylesheet" href="Public/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="Public/css/awesome/css/all.css"/>
</head>
<body>

<div class="container">
    <?php
    include "Views/parts/menu.php"
    ?>

    <h1>
        Edition d'une catégorie <?php echo($category->getLabel()); ?>
    </h1>

    <a href="index.php?controller=category&action=list">Retour</a>
    <form method="post">
        <label for="label_form">Nom de la catégorie</label>
        <input id="label_form" value="<?php echo($category->getLabel());?>" class="form-control" name="label" placeholder="Nom de la catégorie">

        <input type="submit" class="btn btn-success">
    </form>

    <?php
    foreach ($errors as $error){
        echo('<div class="alert alert-danger" role="alert">
  '.$error.'
</div>');
    }
    ?>
</div>

<script rel="script" src="Public/js/bootstrap.min.js"></script>
</body>
</html>