<html>
<head>
    <link rel="stylesheet" href="Public/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="Public/css/awesome/css/all.css"/>
</head>
<body>
<div class="container">
    <?php
    include "Views/parts/menu.php"
    ?>

<a href="index.php?controller=category&action=list">Retour au listing</a>

<!-- J'affiche le label de ma catégorie en passant par le getteurs (attribut label => privé) -->
<h1>Bienvenue sur la page de la catégorie <?php echo($categ->getLabel());?></h1>

</div>

<script rel="script" src="Public/js/bootstrap.min.js"></script>
</body>
</html>