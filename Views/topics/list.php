<html>
<head>
    <link rel="stylesheet" href="Public/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="Public/css/awesome/css/all.css"/>
</head>
<body>
<div class="container">
    <?php
    include "Views/parts/menu.php"
    ?>
    <h1>La liste de mes catégories !</h1>

    <a href="index.php?controller=topic&action=ajout">Ajouter une catégorie</a>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Auteur</th>
            <th scope="col">Titre</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        <?php
        // Je parcours mon tableau d'objet pour afficher une ligne à chaque fois
        foreach ($topics as $topic){
            // On utilise les getteurs pour accèder aux attributs privé
            echo(' <tr>
                <th scope="row">'.$topic->getId().'</th>
                <td>'.$topic->getAuthor().'</td>
                 <td>'.$topic->getTitre().'</td>
                <td>
                    <a title="Supprimer"  href="index.php?controller=topic&action=delete&id='.$topic->getId().'">
                        <i class="fa fa-trash"></i>
                    </a>
                    
                       <a title="Détail"  href="index.php?controller=topic&action=detail&id='.$topic->getId().'">
                        <i class="fa fa-eye"></i>
                    </a>
                    
                      <a title="Editer"  href="index.php?controller=topic&action=edit&id='.$topic->getId().'">
                        <i class="fa fa-edit"></i>
                    </a>
                </td>
            </tr>');
        }
        ?>

        </tbody>
    </table>
</div>

<script rel="script" src="Public/js/bootstrap.min.js"></script>
</body>
</html>