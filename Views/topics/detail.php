<html>
<head>
    <link rel="stylesheet" href="Public/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="Public/css/awesome/css/all.css"/>
</head>
<body>
<div class="container">
    <?php
    include "Views/parts/menu.php"
    ?>

    <a href="index.php?controller=topic&action=list">Retour au listing</a>

    <!-- J'affiche le label de ma catégorie en passant par le getteurs (attribut label => privé) -->
    <h1> <?php echo($topic->getTitre());?> </h1>

    <h2><u>Ecrit par <?php echo($topic->getAuthor()) . ' le '. $topic->getDateAdd()->format('d/m/Y'); ?></u></h2>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <img src="Public/uploads/<?php echo($topic->getImage());?>">
            </div>
            <div class="col-md-6"><?php echo($topic->getContenu());?></div>
        </div>
    </div>
</div>

<script rel="script" src="Public/js/bootstrap.min.js"></script>
</body>
</html>