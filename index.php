<?php
    // Routeur de notre application que toutes requêtes sont adressées à ce fichier (index.php)


    // Ici on inclu toutes classes que l'on utilisera dans notre application
    require 'loader.php';

    // Si l'utilisateur accède à index.php, on le redirige
    // vers la page par défaut (ci-dessous le listing des catégories)
    if(empty($_GET)){
        header('Location: index.php?controller=category&action=list');
    }

    // Je vérifie la variable $_GET["controller"]
    // Si elle est égale à category, je cré un nouvel objet
    // CategoryController
    if($_GET["controller"] == 'category'){
        $controller = new CategoryController();

        // Je vérifie la variable get action
        // Si elle est égale à list, j'appèl la methode
        // listCategory de mon objet CategoryController
        // l'url de cette action sera : index.php?controller=category&action=list
        if($_GET["action"] == 'list'){
            $controller->listCategory();
        }

        // Je vérifie ma variable $_GET["action"] si elle est égale à delete
        // Je vérifie que dans les paramètres GET, j'ai bien un attribut id
        // l'url de cette action sera : index.php?controller=category&action=delete&id=1
        if($_GET["action"] == 'delete' && array_key_exists("id",$_GET)){
            // J'appel la méthode deleteCategory($id) avec $id = au paramètre de la requête
            $controller->deleteCategory($_GET["id"]);
        }

        // Je vérifie ma variable $_GET["action"] si elle est égale à ajout
        // J'appel ensuite la méthode form de mon controller
        // url d'appel : index.php?controller=category&action=ajout
        // On pourra dans ce cas avec une requête de type get ou post
        if($_GET["action"] == 'ajout'){

            $controller->form();
        }

        // On passe dans cette condition pour afficher le détail d'un élément
        // On vérifie qu'on a bien un id l'url afin de savoir quelle ressource
        // on doit afficher
        // URL d'accès : index.php?controller=category&action=detail&id=1
        if($_GET["action"] == 'detail' && array_key_exists("id", $_GET)){
            // On appel la méthode detail du controller CategoryController
            // Elle prend en paramètre l'id envoyé dans l'URL
            $controller->detail($_GET["id"]);
        }

        // Si notre action est égale à edit et que l'on a bien un id dans l'URL
        if($_GET["action"] == 'edit' && array_key_exists("id", $_GET)){
            // Appeler la méthode edit de notre controller
            // Elle prend en paramétre l'id de l'élément que l'on souhaite afficher
            $controller->edit($_GET["id"]);
        }
    }

    if($_GET["controller"] == 'topic'){
        $controller = new TopicController();

        if($_GET["action"] == 'list'){
            $controller->listTopic();
        }

        if($_GET["action"] == 'detail' && array_key_exists("id", $_GET)){
            $controller->detail($_GET["id"]);
        }

        if($_GET["action"] == "delete" && array_key_exists("id", $_GET)){
            $controller->delete($_GET["id"]);
        }

        if($_GET["action"] == "ajout"){
            $controller->add();
        }
    }

    if($_GET["controller"] == 'security'){
        $controller = new SecurityController();
        if($_GET["action"] == 'register'){
            $controller->register();
        }

        if($_GET["action"] == "login"){
            $controller->login();
        }

        if($_GET["action"] == "logout"){
            $controller->logout();
        }
    }


?>